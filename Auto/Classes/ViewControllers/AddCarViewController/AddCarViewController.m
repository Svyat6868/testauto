//
//  AddCarViewController.m
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//


#import "AddCarViewController.h"
#import "ImageCollectionViewCell.h"
#import "AddImageCollectionViewCell.h"

#import "UIViewController+Text.h"
#import "ImageHelper.h"

static CGFloat const kTimeAnimation = 0.35f;

static NSInteger const kTagEngineButton = 1;
static NSInteger const kTagTransmissionButton = 2;
//static NSInteger const kTagConditionButton = 3;

@interface AddCarViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UITextField *modelTextField;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;

@property (weak, nonatomic) IBOutlet UILabel *engineLabel;
@property (weak, nonatomic) IBOutlet UITextField *engineTextField;

@property (weak, nonatomic) IBOutlet UILabel *transmissionLabel;
@property (weak, nonatomic) IBOutlet UITextField *transmissionTextFied;

@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@property (weak, nonatomic) IBOutlet UITextField *conditionTextField;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (strong, nonatomic) UIPickerView *typePickerView;
@property (strong, nonatomic) UITextField *currentTextField;

@property (strong, nonatomic) NSMutableArray *images;

@property (assign, nonatomic) SPCarEngineType carEngineType;
@property (assign, nonatomic) SPCarTransmissionType carTransmissionType;
@property (assign, nonatomic) SPCarStateType carStateType;

@end

@implementation AddCarViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.images = [NSMutableArray new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configNavigation];
    [self configViewController];
    [self configLocalization];
    [self configTypePicker];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IBAction

- (IBAction)prevButtonAction:(id)sender
{
    CGPoint position = self.collectionView.contentOffset;
    position.x = position.x - CGRectGetWidth(self.collectionView.frame);
    if (position.x < 0) {
        position.x = 0;
    }
    [self.collectionView setContentOffset:position animated:YES];
}

- (IBAction)nextButtonAction:(id)sender
{
    CGPoint position = self.collectionView.contentOffset;
    position.x = position.x + CGRectGetWidth(self.collectionView.frame);
    if (position.x > (self.collectionView.contentSize.width - CGRectGetWidth(self.collectionView.frame))) {
        position.x = self.collectionView.contentSize.width - CGRectGetWidth(self.collectionView.frame);
    }
    if (position.x < 0) {
        position.x = 0;
    }
    
    [self.collectionView setContentOffset:position animated:YES];
}

- (IBAction)typeButtonAction:(id)sender
{
    switch (((UIButton *)sender).tag) {
        case kTagEngineButton:
            [self.engineTextField becomeFirstResponder];
            break;
        case kTagTransmissionButton:
            [self.transmissionTextFied becomeFirstResponder];
            break;
        default:
            [self.conditionTextField becomeFirstResponder];
            break;
    }
}

- (IBAction)donePriceAction:(id)sender
{
    [self.priceTextField resignFirstResponder];
}

- (IBAction)typeDoneInputAction:(id)sender
{
    NSInteger row = [self.typePickerView selectedRowInComponent:0];
    
    if ([self.currentTextField isEqual:self.engineTextField]) {
        self.currentTextField.text = [CarHelper engineTypeStringWithEngineType:row];
        self.carEngineType = row;
    } else if ([self.currentTextField isEqual:self.transmissionTextFied]) {
        self.currentTextField.text = [CarHelper transTypeStringWithTransnType:row];
        self.carTransmissionType = row;
    } else {
        self.currentTextField.text = [CarHelper stateTypeStringWithStateType:row];
        self.carStateType = row;
    }
    
    [self.currentTextField resignFirstResponder];
}

- (IBAction)doneDescriptionTextViewAction:(id)sender
{
    [self.descriptionTextView resignFirstResponder];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveCarAction:(id)sender
{
    if (([self.modelTextField.text length] > 0) && ([self.priceTextField.text length] > 0) &&([self.engineTextField.text length] > 0) && ([self.transmissionTextFied.text length] > 0) && ([self.conditionTextField.text length] > 0)) {
        
        if ([self.images count] > 0) {
            
            NSString *className = NSStringFromClass([CarModel class]);
            CarModel *newCar = (CarModel *)[[CoreDataManager sharedInstance] createManagedObjectWithClassName:className];
            
            newCar.id = [CarHelper newIdCar];
            newCar.model = self.modelTextField.text;
            newCar.price = [NSNumber numberWithInteger:[self.priceTextField.text integerValue]];
            newCar.engineType = [NSNumber numberWithInteger:self.carEngineType];
            newCar.transmissionType = [NSNumber numberWithInteger:self.carTransmissionType];
            newCar.stateType = [NSNumber numberWithInteger:self.carStateType];
            newCar.carDescription = self.descriptionTextView.text;
            
            [ImageHelper setImages:self.images toCar:newCar];
            [[CoreDataManager sharedInstance] addManagedObject:newCar];
            
            if ([self.delegate respondsToSelector:@selector(addCarViewController:car:)]) {
                [self.delegate addCarViewController:self car:newCar];
            }
            
        } else {
            [APP_DELEGATE showAlertWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Please add a photos", nil) viewController:self];
        }
    } else {
        [APP_DELEGATE showAlertWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Please fill in all blank fields", nil) viewController:self];
    }
}

#pragma mark - Private 

- (void)configNavigation
{
    self.navigationItem.title = NSLocalizedString(@"AddCarViewControllerTitle", nil);
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItems = @[[SPNavigationItemFactory createLeftAdjustingItem], [SPNavigationItemFactory createBackItem:self selector:@selector(backAction:) itemColor:[UIColor whiteColor]]];
    self.navigationItem.rightBarButtonItems = @[[SPNavigationItemFactory createRightAdjustingItem], [SPNavigationItemFactory createAddCarItem:self selector:@selector(saveCarAction:) itemColor:[UIColor whiteColor]]];
}

- (void)configViewController
{
    [self addInputAccessoryViewInTextField:self.priceTextField target:self action:@selector(donePriceAction:)];
    [self addInputAccessoryViewInTextField:self.engineTextField target:self action:@selector(typeDoneInputAction:)];
    [self addInputAccessoryViewInTextField:self.transmissionTextFied target:self action:@selector(typeDoneInputAction:)];
    [self addInputAccessoryViewInTextField:self.conditionTextField target:self action:@selector(typeDoneInputAction:)];
    
    [self addInputAccessoryViewInTextView:self.descriptionTextView target:self action:@selector(doneDescriptionTextViewAction:)];
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class])];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([AddImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([AddImageCollectionViewCell class])];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)configLocalization
{
    self.modelLabel.text = NSLocalizedString(@"AddCarViewControllerCarLabel", nil);
    self.modelTextField.placeholder = NSLocalizedString(@"AddCarViewControllerCarPalceholder", nil);
    
    self.priceLabel.text = NSLocalizedString(@"AddCarViewControllerPriceLabel", nil);
    self.priceTextField.placeholder = NSLocalizedString(@"AddCarViewControllerPricePalceholder", nil);
    
    self.engineLabel.text = NSLocalizedString(@"AddCarViewControllerEngineLabel", nil);
    self.engineTextField.placeholder = NSLocalizedString(@"AddCarViewControllerEnginePalceholder", nil);
    
    self.transmissionLabel.text = NSLocalizedString(@"AddCarViewControllerTransLabel", nil);
    self.transmissionTextFied.placeholder = NSLocalizedString(@"AddCarViewControllerTransPalceholder", nil);
    
    self.conditionLabel.text = NSLocalizedString(@"AddCarViewControllerConditionLabel", nil);
    self.conditionTextField.placeholder = NSLocalizedString(@"AddCarViewControllerConditionPalceholder", nil);
    
    self.descriptionLabel.text = NSLocalizedString(@"AddCarViewControllerDescriptionLabel", nil);
    self.descriptionTextView.text = @"";
}

- (void)configTypePicker
{
    self.typePickerView = [UIPickerView new];
    self.typePickerView.dataSource = self;
    self.typePickerView.delegate = self;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.images count] + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.images count]) {
        AddImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([AddImageCollectionViewCell class]) forIndexPath:indexPath];
        return cell;
    } else {
        ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class]) forIndexPath:indexPath];
        cell.imageView.image = self.images[indexPath.row];
        return cell;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.images count]) {
        
        UIImagePickerController *imagePickerController = [UIImagePickerController new];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.delegate = self;
        
        typeof(self) __weak weakself = self;
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"AddCarViewControllerPhoto", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *actionCamera = [UIAlertAction actionWithTitle:NSLocalizedString(@"AddCarViewControllerCameraSource", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [weakself presentViewController:imagePickerController animated:YES completion:nil];
            } else {
                [APP_DELEGATE showAlertWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"AddCarViewControllerCameraNotWork", nil) viewController:weakself];
            }
        }];
        
        UIAlertAction *actiuonCameraRow = [UIAlertAction actionWithTitle:NSLocalizedString(@"AddCarViewControllerCameraRollSource", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [weakself presentViewController:imagePickerController animated:YES completion:nil];
        }];
        
        UIAlertAction *actiuonCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"AddCarViewControllerPhotoCancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [alertController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertController addAction:actionCamera];
        [alertController addAction:actiuonCameraRow];
        [alertController addAction:actiuonCancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([self.currentTextField isEqual:self.engineTextField]) {
        return kSPCarEngineTypeCountItems;
    } else if ([self.currentTextField isEqual:self.transmissionTextFied]) {
        return kSPCarTransmissionTypeCountItems;
    } else {
        return kSPCarStateTypeCountItems;
    }
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([self.currentTextField isEqual:self.engineTextField]) {
        return [CarHelper engineTypeStringWithEngineType:row];
    } else if ([self.currentTextField isEqual:self.transmissionTextFied]) {
        return [CarHelper transTypeStringWithTransnType:row];
    } else {
        return [CarHelper stateTypeStringWithStateType:row];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([self.engineTextField isEqual:textField] || [self.transmissionTextFied isEqual:textField] || [self.conditionTextField isEqual:textField]) {
        
        self.currentTextField = textField;
        [self.typePickerView reloadComponent:0];
        textField.inputView = self.typePickerView;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    UIEdgeInsets contentInset = self.scrollView.contentInset;
    CGFloat yPosition = self.scrollView.contentSize.height - CGRectGetHeight(self.scrollView.frame) - contentInset.bottom;
    [self.scrollView setContentOffset:CGPointMake(0, -yPosition) animated:YES];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:^{
        picker.delegate = nil;
    }];
    
    [self.images addObject:image];
    [self.collectionView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:^{
        picker.delegate = nil;
    }];
}

#pragma mark - Keyboard NSNotification

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *keyboardInfo = [notification userInfo];
    CGRect keyboardFrame = [keyboardInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];

    UIEdgeInsets contentInset = self.scrollView.contentInset;
    contentInset.bottom = CGRectGetHeight(keyboardFrame);
    
    [UIView animateWithDuration:kTimeAnimation animations:^{
        self.scrollView.contentInset = contentInset;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:kTimeAnimation animations:^{
        self.scrollView.contentInset = UIEdgeInsetsZero;
    }];
}

@end
