//
//  AddCarViewController.h
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddCarViewControllerDelegate;

@interface AddCarViewController : UIViewController

@property (weak, nonatomic) id <AddCarViewControllerDelegate> delegate;

@end

@protocol AddCarViewControllerDelegate <NSObject>

- (void)addCarViewController:(AddCarViewController *)addCarViewController car:(CarModel *)car;

@end