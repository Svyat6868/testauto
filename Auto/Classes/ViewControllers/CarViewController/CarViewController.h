//
//  CarViewController.h
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarViewController : UIViewController

- (instancetype)initWithCar:(CarModel *)car;

@end
