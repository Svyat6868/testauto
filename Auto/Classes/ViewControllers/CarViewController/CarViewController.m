//
//  CarViewController.m
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "CarViewController.h"
#import "ImageCollectionViewCell.h"

@interface CarViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *imagesPageControl;

@property (weak, nonatomic) IBOutlet UILabel *modelDLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceDLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UILabel *engineDLabel;
@property (weak, nonatomic) IBOutlet UILabel *engineLabel;

@property (weak, nonatomic) IBOutlet UILabel *transmissionDLabel;
@property (weak, nonatomic) IBOutlet UILabel *transmissionLabel;

@property (weak, nonatomic) IBOutlet UILabel *conditionDLabel;
@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;

@property (weak, nonatomic) IBOutlet UILabel *descriptionDLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (strong, nonatomic) CarModel *car;

@end

@implementation CarViewController

- (instancetype)initWithCar:(CarModel *)car
{
    self = [super init];
    if (self) {
        self.car = car;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNavigation];
    [self configLocalization];
    [self configViewController];
}

#pragma mark - IBAction

- (IBAction)prevButtonAction:(id)sender
{
    CGPoint position = self.collectionView.contentOffset;
    position.x = position.x - CGRectGetWidth(self.collectionView.frame);
    if (position.x < 0) {
        position.x = 0;
    }
    [self.collectionView setContentOffset:position animated:YES];
    
    NSInteger prevPage = self.imagesPageControl.currentPage - 1;
    self.imagesPageControl.currentPage = prevPage >= 0 ? prevPage : 0;
}

- (IBAction)nextButtonAction:(id)sender
{
    CGPoint position = self.collectionView.contentOffset;
    position.x = position.x + CGRectGetWidth(self.collectionView.frame);
    if (position.x > (self.collectionView.contentSize.width - CGRectGetWidth(self.collectionView.frame))) {
        position.x = self.collectionView.contentSize.width - CGRectGetWidth(self.collectionView.frame);
    }
    if (position.x < 0) {
        position.x = 0;
    }
    
    [self.collectionView setContentOffset:position animated:YES];
    
    NSInteger nextPage = self.imagesPageControl.currentPage + 1;
    self.imagesPageControl.currentPage = nextPage < [self.car.images count] ? nextPage : [self.car.images count] - 1;
}

- (IBAction)changeImagesPageControl:(id)sender
{
    CGPoint position = CGPointZero;
    position.x = self.collectionView.frame.size.width * self.imagesPageControl.currentPage;
    [self.collectionView setContentOffset:position animated:YES];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.car.images count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class]) forIndexPath:indexPath];
    cell.imageView.image = self.car.images[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.frame.size;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([self.collectionView isEqual:scrollView]) {
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = floor((scrollView.contentOffset.x - pageWidth) / pageWidth) + 1;
        self.imagesPageControl.currentPage = page;
    }
}

#pragma mark - Private

- (void)configNavigation
{
    self.navigationItem.title = NSLocalizedString(@"CarViewControllerTitle", nil);
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItems = @[[SPNavigationItemFactory createLeftAdjustingItem], [SPNavigationItemFactory createBackItem:self selector:@selector(backAction:) itemColor:[UIColor whiteColor]]];
}

- (void)configLocalization
{
    self.modelDLabel.text = NSLocalizedString(@"CarViewControllerCarLabel", nil);
    self.priceDLabel.text = NSLocalizedString(@"CarViewControllerPriceLabel", nil);
    self.engineDLabel.text = NSLocalizedString(@"CarViewControllerEngineLabel", nil);
    self.transmissionDLabel.text = NSLocalizedString(@"CarViewControllerTransLabel", nil);
    self.conditionDLabel.text = NSLocalizedString(@"CarViewControllerConditionLabel", nil);
    self.descriptionDLabel.text = NSLocalizedString(@"CarViewControllerDescriptionLabel", nil);
}

- (void)configViewController
{
    self.modelLabel.text = self.car.model;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld", [self.car.price integerValue]];
    self.engineLabel.text = [CarHelper engineTypeStringWithEngineType:[self.car.engineType integerValue]];
    self.transmissionLabel.text = [CarHelper transTypeStringWithTransnType:[self.car.transmissionType integerValue]];
    self.conditionLabel.text = [CarHelper stateTypeStringWithStateType:[self.car.stateType integerValue]];
    self.descriptionLabel.text = self.car.carDescription;
    self.imagesPageControl.numberOfPages = [self.car.images count];

    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class])];
}

@end
