//
//  ListWeatherViewController.m
//  Auto
//
//  Created by Svyat on 8/1/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "ListWeatherViewController.h"
#import "WeatherTableViewCell.h"

static CGFloat const kHeightWeatherCell = 150.f;

@interface ListWeatherViewController ()

@property (weak, nonatomic) IBOutlet UITableView *weatherTableView;

@property (strong, nonatomic) NSArray *listWeathers;

@end

@implementation ListWeatherViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString *className = NSStringFromClass([WeatherModel class]);
        self.listWeathers = [[CoreDataManager sharedInstance] getManagedObjectsFromClassName:className];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configViewController];
}

#pragma mark - IBAction

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private

- (void)configViewController
{
    self.navigationItem.title = NSLocalizedString(@"ListWeatherViewControllerTitle", nil);
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItems = @[[SPNavigationItemFactory createLeftAdjustingItem], [SPNavigationItemFactory createBackItem:self selector:@selector(backAction:) itemColor:[UIColor whiteColor]]];
    
    [self.weatherTableView registerNib:[UINib nibWithNibName:NSStringFromClass([WeatherTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([WeatherTableViewCell class])];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listWeathers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeatherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([WeatherTableViewCell class]) forIndexPath:indexPath];
    
    NSInteger indexWeather = [self.listWeathers count] - indexPath.row - 1;
    WeatherModel *weather = self.listWeathers[indexWeather];
    [(WeatherTableViewCell *)cell fillCellWithWeather:weather];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kHeightWeatherCell;
}

@end
