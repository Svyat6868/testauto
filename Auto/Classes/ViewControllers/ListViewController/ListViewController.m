//
//  ListViewController.m
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "ListViewController.h"
#import "AddCarViewController.h"
#import "CarViewController.h"
#import "ListWeatherViewController.h"

#import "WeatherTableViewCell.h"
#import "CarTableViewCell.h"
#import "ImageHelper.h"

static NSString *const kListViewControllerTitle = @"ListViewControllerTitle";

static CGFloat const kHeightWeatherCell = 150.f;
static CGFloat const kHeightCarCell = 80.f;

@interface ListViewController () <UITableViewDataSource, UITableViewDelegate, AddCarViewControllerDelegate, WeatherManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) NSMutableArray *carList;

@end

@implementation ListViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString *className = NSStringFromClass([CarModel class]);
        NSArray *cars = [[CoreDataManager sharedInstance] getManagedObjectsFromClassName:className];
        self.carList = [cars mutableCopy];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configViewController];
}

#pragma mark - Private

- (void)configViewController
{
    self.navigationItem.title = NSLocalizedString(kListViewControllerTitle, nil);
    
    self.navigationItem.rightBarButtonItems = @[[SPNavigationItemFactory createRightAdjustingItem], [SPNavigationItemFactory createAddItem:self selector:@selector(addCarAction:) itemColor:[UIColor whiteColor]]];
    
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([WeatherTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([WeatherTableViewCell class])];
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([CarTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CarTableViewCell class])];
    
    [[WeatherManager sharedInstance] addListener:self];
}

#pragma mark - IBAction

- (IBAction)addCarAction:(id)sender
{
    AddCarViewController *addCarViewController = [AddCarViewController new];
    addCarViewController.delegate = self;
    [self.navigationController pushViewController:addCarViewController animated:YES];
}


#pragma mark - AddCarViewControllerDelegate

- (void)addCarViewController:(AddCarViewController *)addCarViewController car:(CarModel *)car
{
    [self.carList addObject:car];
    [self.navigationController popViewControllerAnimated:YES];
    [self.listTableView reloadData];
}


#pragma mark - WeatherManagerDelegate

- (void)weatherManager:(WeatherManager *)weatherManager
{
    NSArray<NSIndexPath *> *indexPaths= [self.listTableView indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in indexPaths) {
        if (indexPath.row == 0) {
            WeatherTableViewCell *cell = [self.listTableView cellForRowAtIndexPath:indexPath];
            [cell fillCellWithWeather:weatherManager.weather];
            
            break;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.carList count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([WeatherTableViewCell class]) forIndexPath:indexPath];
        
        WeatherModel *weather = [WeatherManager sharedInstance].weather;
        [(WeatherTableViewCell *)cell fillCellWithWeather:weather];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CarTableViewCell class]) forIndexPath:indexPath];
        CarModel *car = self.carList[indexPath.row - 1];
        [(CarTableViewCell *)cell fillCellWithCar:car];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        ListWeatherViewController *listWeatherViewController = [[ListWeatherViewController alloc] init];
        [self.navigationController pushViewController:listWeatherViewController animated:YES];
    } else {
        CarModel *car = self.carList[indexPath.row - 1];
        CarViewController *carViewController = [[CarViewController alloc] initWithCar:car];
        [self.navigationController pushViewController:carViewController animated:YES];
    }
}

- (UITableViewCellAccessoryType)tableView:(UITableView *)tableView accessoryTypeForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > 0) {
        return UITableViewCellAccessoryDisclosureIndicator;
    } else {
        return UITableViewCellAccessoryNone;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return kHeightWeatherCell;
    } else {
        return kHeightCarCell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row > 0;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger indexCar = indexPath.row - 1;
    CarModel *car = self.carList[indexCar];
    
    [self.carList removeObject:car];
    [ImageHelper deleteImagesFromCar:car];
    [[CoreDataManager sharedInstance] deleteManagedObject:car];
    
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
}

@end
