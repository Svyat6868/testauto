//
//  SPExtensionView.h
//  Auto
//
//  Created by Свят on 12/20/15.
//  Copyright © 2015 Svyat. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SFBorder) {
    SFBorderLeft        = 1,
    SFBorderRight       = 1 << 1,
    SFBorderTop         = 1 << 2,
    SFBorderBottom      = 1 << 3
};

IB_DESIGNABLE
@interface SPExtensionView : UIView

@property (assign, nonatomic) IBInspectable NSInteger borders;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

@end
