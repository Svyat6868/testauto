//
//  SPExtensionView.m
//  Auto
//
//  Created by Свят on 12/20/15.
//  Copyright © 2015 Svyat. All rights reserved.
//

#import "SPExtensionView.h"

@implementation SPExtensionView

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    
    CGContextSetFillColorWithColor(context, self.borderColor.CGColor);
    
    if (self.borders & SFBorderLeft) {
        CGRect leftBorder = CGRectMake(0.f, 0.f, self.borderWidth, CGRectGetHeight(self.frame));
        CGContextFillRect(context, leftBorder);
    }
    if (self.borders & SFBorderRight) {
        CGRect rightBorder = CGRectMake(CGRectGetWidth(self.frame) - self.borderWidth, 0.f, self.borderWidth, CGRectGetHeight(self.frame));
        CGContextFillRect(context, rightBorder);
    }
    if (self.borders & SFBorderTop) {
        CGRect topRect = CGRectMake(0.f, 0.f, CGRectGetWidth(self.frame), self.borderWidth);
        CGContextFillRect(context, topRect);
    }
    if (self.borders & SFBorderBottom) {
        CGRect bottomBorder = CGRectMake(0.f, CGRectGetHeight(self.frame) - self.borderWidth, CGRectGetWidth(self.frame), self.borderWidth);
        CGContextFillRect(context, bottomBorder);
    }
    
    CGContextRestoreGState(context);
}

#pragma mark - Set

- (void)setBorders:(NSInteger)borders
{
    _borders = borders;
    [self setNeedsDisplay];
}

@end
