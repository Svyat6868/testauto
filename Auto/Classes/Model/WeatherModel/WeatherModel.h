//
//  WeatherObjectModel.h
//  Auto
//
//  Created by Svyat on 7/31/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef NS_ENUM(NSInteger, SPWeatherType) {
    SPWeatherTypeRain,
    SPWeatherTypeClear,
    SPWeatherTypeClouds,
    SPWeatherTypeSnow
};

@interface WeatherModel : NSManagedObject

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *place;
@property (nullable, nonatomic, retain) NSString *precipitation;
@property (nullable, nonatomic, retain) NSNumber *weatherType;
@property (nullable, nonatomic, retain) NSNumber *temperature;

@property (nullable, nonatomic, retain) NSDate *date;

@end

@interface WeatherModel (Extensions)

- (nullable UIImage *)weatherTypeImage;
- (nullable NSString *)weatherTypeString;
- (SPWeatherType)getSPWeatherType;

@end
