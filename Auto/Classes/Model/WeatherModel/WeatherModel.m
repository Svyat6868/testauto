//
//  WeatherObjectModel.m
//  Auto
//
//  Created by Svyat on 7/31/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "WeatherModel.h"

static NSString *const kWeatherTypeRain = @"SPWeatherTypeRain";
static NSString *const kWeatherTypeClear = @"SPWeatherTypeClear";
static NSString *const kWeatherTypeClouds = @"SPWeatherTypeClouds";
static NSString *const kWeatherTypeSnow = @"SPWeatherTypeSnow";

@implementation WeatherModel

@dynamic id;
@dynamic place;
@dynamic precipitation;
@dynamic weatherType;
@dynamic temperature;
@dynamic date;


#pragma mark - Public

- (UIImage *)weatherTypeImage
{
    switch ([self.weatherType integerValue]) {
        case SPWeatherTypeRain:
            return [UIImage imageNamed:@"weatherRainImage"];
        case SPWeatherTypeClear:
            return [UIImage imageNamed:@"weatherSunnyImage"];
        case SPWeatherTypeClouds:
            return [UIImage imageNamed:@"weatherCloudyImage"];
        default:
            return [UIImage imageNamed:@"weatherSnowImage"];
    }
}

- (NSString *)weatherTypeString
{
    switch ([self.weatherType integerValue]) {
        case SPWeatherTypeRain:
            return NSLocalizedString(kWeatherTypeRain, nil);
        case SPWeatherTypeClear:
            return NSLocalizedString(kWeatherTypeClear, nil);
        case SPWeatherTypeClouds:
            return NSLocalizedString(kWeatherTypeClouds, nil);
        default:
            return NSLocalizedString(kWeatherTypeSnow, nil);
    }
}

- (SPWeatherType)getSPWeatherType
{
    return [self.weatherType integerValue];
}

@end
