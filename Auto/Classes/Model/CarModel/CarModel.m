//
//  CarModel.m
//  Auto
//
//  Created by Svyat on 7/31/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "CarModel.h"
#import "ImageHelper.h"

@interface CarModel()

@property (strong, nonatomic) NSArray * _Nonnull privateImages;

@end

@implementation CarModel

@dynamic id;
@dynamic model;
@dynamic price;
@dynamic engineType;
@dynamic transmissionType;
@dynamic stateType;
@dynamic carDescription;

@synthesize privateImages;

#pragma mark - Public

- (SPCarEngineType)getSPEngineType
{
    return [self.engineType integerValue];
}

- (SPCarTransmissionType)getSPTransmissionType
{
    return [self.transmissionType integerValue];
}

- (SPCarStateType)getSPStateType
{
    return [self.stateType integerValue];
}

- (nonnull NSArray *)images
{
    if (self.privateImages == nil) {
        self.privateImages = [ImageHelper imagesToCar:self];
    }
    
    return self.privateImages;
}

#pragma mark - Private

- (void)setCarImages:(nonnull NSArray *)carImages
{
    self.privateImages = carImages;
}

@end


