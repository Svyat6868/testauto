//
//  CarModel.h
//  Auto
//
//  Created by Svyat on 7/31/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CarHelper.h"

@interface CarModel : NSManagedObject

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *model;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSNumber *engineType;
@property (nullable, nonatomic, retain) NSNumber *transmissionType;
@property (nullable, nonatomic, retain) NSNumber *stateType;
@property (nullable, nonatomic, retain) NSString *carDescription;


@end

@interface CarModel (Extensions)

- (SPCarEngineType)getSPEngineType;
- (SPCarTransmissionType)getSPTransmissionType;
- (SPCarStateType)getSPStateType;

- (nonnull NSArray *)images;

@end
