//
//  ImageHelper.h
//  Auto
//
//  Created by Boost Ser on 1/11/18.
//  Copyright © 2018 Svyat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CarModel.h"

@interface ImageHelper : NSObject

+ (void)addImage:(nonnull UIImage *)image toCar:(nonnull CarModel *)car;
+ (nonnull NSArray *)imagesToCar:(nonnull CarModel *)car;
+ (void)setImages:(nonnull NSMutableArray *)images toCar:(nonnull CarModel *)car;
+ (void)deleteImagesFromCar:(nonnull CarModel *)car;

@end
