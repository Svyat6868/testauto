//
//  ImageHelper.m
//  Auto
//
//  Created by Boost Ser on 1/11/18.
//  Copyright © 2018 Svyat. All rights reserved.
//

#import "ImageHelper.h"
#import "CarModel+Private.h"

@implementation ImageHelper

+ (void)addImage:(nonnull UIImage *)image toCar:(nonnull CarModel *)car
{
    NSMutableArray *carImages = [car.images mutableCopy];
    
    NSURL *pathDirectoryURL = [ImageHelper pathToCarImages:car];
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:pathDirectoryURL includingPropertiesForKeys:[NSArray arrayWithObjects:NSURLNameKey, nil] options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    
    NSString *nameFile = [NSString stringWithFormat:@"%ld", (unsigned long)[dirContents count]];
    NSURL *fileURL = [pathDirectoryURL URLByAppendingPathComponent:nameFile];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    [imageData writeToURL:fileURL atomically:YES];
    
    [carImages addObject:image];
    [car setCarImages:[carImages copy]];
}

+ (nonnull NSArray *)imagesToCar:(nonnull CarModel *)car
{
    NSURL *pathDirectoryURL = [ImageHelper pathToCarImages:car];
    
    NSArray *filesUrl = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:pathDirectoryURL includingPropertiesForKeys:[NSArray arrayWithObjects:NSURLNameKey, NSURLIsDirectoryKey, NSURLContentModificationDateKey, nil] options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    
    NSMutableArray *images = [NSMutableArray new];
    for (NSURL *imageUrl in filesUrl) {
        UIImage *image = [ImageHelper getImageFromUrlImage:imageUrl];
        [images addObject:image];
    }
    
    return [images copy];
}

+ (void)setImages:(nonnull NSMutableArray *)images toCar:(nonnull CarModel *)car
{
    for (UIImage *image in images) {
        [ImageHelper addImage:image toCar:car];
    }
    
    [car setCarImages:[images copy]];
}

+ (void)deleteImagesFromCar:(nonnull CarModel *)car
{
    NSURL *pathDirectoryURL = [ImageHelper pathToCarImages:car];
    [[NSFileManager defaultManager] removeItemAtURL:pathDirectoryURL error:nil];
    
    [car setCarImages:[NSArray new]];
}

#pragma mark - Private

+ (NSURL *)pathToCarImages:(CarModel *)car
{
    NSURL *documentsDirectoryURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    NSString *nameDirectory = [NSString stringWithFormat:@"%@", car.id];
    NSURL *imageSubdirectoryURL = [documentsDirectoryURL URLByAppendingPathComponent:nameDirectory];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[imageSubdirectoryURL path] isDirectory:nil]) {
        NSError *error;
        
        [[NSFileManager defaultManager]createDirectoryAtURL:imageSubdirectoryURL
                                withIntermediateDirectories:YES
                                                 attributes:nil
                                                      error:&error];
    }
    
    return imageSubdirectoryURL;
}

+ (UIImage *)getImageFromUrlImage:(NSURL *)urlImage
{
    UIImage *image = [UIImage imageNamed:[urlImage absoluteString]];
    if (!image) {
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        image = [UIImage imageWithData:imageData];
    }
    return image;
}

@end
