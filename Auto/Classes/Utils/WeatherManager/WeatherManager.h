//
//  WeatherManager.h
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "WeatherModel.h"

@protocol WeatherManagerDelegate;

@interface WeatherManager : NSObject

@property (strong, nonatomic) WeatherModel *weather;

+ (instancetype)sharedInstance;

- (void)addListener:(id <WeatherManagerDelegate>)listener;

@end

@protocol WeatherManagerDelegate <NSObject>

- (void)weatherManager:(WeatherManager *)weatherManager;

@end
