//
//  WeatherManager.m
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "WeatherManager.h"
#import "OWMWeatherAPI.h"
#import "Reachability.h"

static NSString *const kAPIKey = @"bc22ce3431f698d31fd46596b8842e64";
static CGFloat const kTimeUpdateInterval = 36000.f;
static NSInteger const kDistanceFilter = 500;

static NSString *const kGoogleUrl = @"www.google.com";
static CLLocationCoordinate2D const kKievCoordinate = {50.44, 30.53};

static NSString *const kKeyIdWeather = @"idWeather";


@interface WeatherManager() <CLLocationManagerDelegate>

@property (strong, nonatomic) OWMWeatherAPI *weatherAPI;
@property (strong, nonatomic) NSTimer *updateTimer;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSHashTable *listeners;

@property (strong, nonatomic) Reachability* reachability;

@end

@implementation WeatherManager

+ (instancetype)sharedInstance
{
    static WeatherManager *weatherManager = nil;
    static dispatch_once_t dispatchOnceToken = 0;
    
    dispatch_once(&dispatchOnceToken, ^{
        weatherManager = [[[self class] alloc] init];
    });
    
    return weatherManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.listeners = [NSHashTable weakObjectsHashTable];
        self.reachability = [Reachability reachabilityWithHostname:kGoogleUrl];
        
        self.weatherAPI = [[OWMWeatherAPI alloc] initWithAPIKey:kAPIKey];
        [self.weatherAPI setTemperatureFormat:kOWMTempCelcius];
        
        self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeUpdateInterval target:self selector:@selector(timeUpdate:) userInfo:nil repeats:YES];
        
        [self configLocation];
    }
    return self;
}


#pragma mark - Public

- (void)addListener:(id <WeatherManagerDelegate>)listener
{
    [self.listeners addObject:listener];
}


#pragma mark - Private 

- (void)configLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kDistanceFilter;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager requestWhenInUseAuthorization];
}

- (WeatherModel *)weatherWithRawWeather:(nonnull NSDictionary *)rawWeather
{
    NSString *className = NSStringFromClass([WeatherModel class]);
    WeatherModel *weather = (WeatherModel *)[[CoreDataManager sharedInstance] createManagedObjectWithClassName:className];
    
    weather.id = [self newIdWeather];
    weather.place = rawWeather[@"name"];
    weather.precipitation = rawWeather[@"rain.3h"];
    
    NSNumber *minTemp = rawWeather[@"main"][@"temp_min"];
    NSNumber *maxTemp = rawWeather[@"main"][@"temp_max"];
    weather.temperature = [NSNumber numberWithInteger:([maxTemp integerValue] + [minTemp integerValue]) / 2];
    
    NSString *weatherTyteString = rawWeather[@"weather"][0][@"main"];
    
    SPWeatherType spWeatherType;
    if ([weatherTyteString isEqualToString:@"Clouds"]) {
        spWeatherType = SPWeatherTypeClouds;
    } else if ([weatherTyteString isEqualToString:@"Clear"]) {
        spWeatherType = SPWeatherTypeClear;
    } else if ([weatherTyteString isEqualToString:@"Rain"]) {
        spWeatherType = SPWeatherTypeRain;
    } else if ([weatherTyteString isEqualToString:@"Snow"]) {
        spWeatherType = SPWeatherTypeSnow;
    }
    
    weather.weatherType = [NSNumber numberWithInteger:spWeatherType];
    weather.date = [NSDate date];
    
    return weather;
}

- (NSNumber *)newIdWeather
{
    NSNumber *idWeather = [[NSUserDefaults standardUserDefaults] objectForKey:kKeyIdWeather];
    NSNumber *newIdWeather = [NSNumber numberWithInteger:[idWeather integerValue] + 1];
    [[NSUserDefaults standardUserDefaults] setObject:newIdWeather forKey:kKeyIdWeather];
    
    return newIdWeather;
}


#pragma mark - Timer

- (void)timeUpdate:(NSTimer *)timer
{
    void (^completion)(WeatherModel *weather) = ^(WeatherModel *weather) {
        self.weather = weather;
        
        for (id listener in self.listeners) {
            if ([listener respondsToSelector:@selector(weatherManager:)]) {
                [listener weatherManager:self];
            }
        }
    };
    
    if (self.reachability.isReachable) {
        CLLocationCoordinate2D locationCoordinate2D = self.locationManager.location != nil ? self.locationManager.location.coordinate : kKievCoordinate;
        
        [self.weatherAPI currentWeatherByCoordinate:locationCoordinate2D withCallback:^(NSError *error, NSDictionary *result) {
            
            if (error == nil) {
                WeatherModel *weather = [self weatherWithRawWeather:result];
                
                [[CoreDataManager sharedInstance] addManagedObject:weather];
                completion(weather);
            }
        }];
    } else {
        NSString *className = NSStringFromClass([WeatherModel class]);
        NSArray *weathers = [[CoreDataManager sharedInstance] getManagedObjectsFromClassName:className];
        WeatherModel *weather = [weathers lastObject];
        completion(weather);
    }
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [self.updateTimer fire];
}

@end











