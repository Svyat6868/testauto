//
//  CarHelper.m
//  Auto
//
//  Created by Svyat on 7/27/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "CarHelper.h"

static NSString *const kKeyIdCar = @"idCar";

static NSString *const kCarEngineType2ie = @"SPCarEngineType2ie";
static NSString *const kCarEngineType2t = @"SPCarEngineType2t";
static NSString *const kCarEngineType2td = @"SPCarEngineType2td";
static NSString *const kCarEngineType1_6ie = @"SPCarEngineType1_6ie";
static NSString *const kCarEngineType1_6t = @"SPCarEngineType1_6t";
static NSString *const kCarEngineType1_6td = @"SPCarEngineType1_6td";

static NSString *const kCarTransmissionTypeManual = @"SPCarTransmissionTypeManual";
static NSString *const kCarTransmissionTypeSemi_automatic = @"SPCarTransmissionTypeSemi_automatic";
static NSString *const kCarTransmissionTypeaAtomatic = @"SPCarTransmissionTypeaAtomatic";

static NSString *const kCarStateTypeGood = @"SPCarStateTypeGood";
static NSString *const kCarStateTypeNormal = @"SPCarStateTypeNormal";
static NSString *const kCarStateTypeBad = @"SPCarStateTypeBad";

@implementation CarHelper

#pragma mark - Public

+ (NSNumber *)newIdCar
{
    NSNumber *idCarNumber = [[NSUserDefaults standardUserDefaults] objectForKey:kKeyIdCar];
    NSNumber *newIdCar = [NSNumber numberWithInteger:[idCarNumber integerValue] + 1];
    [[NSUserDefaults standardUserDefaults] setObject:newIdCar forKey:kKeyIdCar];
    
    return newIdCar;
}


#pragma mark - Public Type

+ (nonnull NSString *)engineTypeStringWithEngineType:(SPCarEngineType)engineType
{
    switch (engineType) {
        case SPCarEngineType2ie:
            return NSLocalizedString(kCarEngineType2ie, nil);
        case SPCarEngineType2t:
            return NSLocalizedString(kCarEngineType2t, nil);
        case SPCarEngineType2td:
            return NSLocalizedString(kCarEngineType2td, nil);
        case SPCarEngineType1_6ie:
            return NSLocalizedString(kCarEngineType1_6ie, nil);
        case SPCarEngineType1_6t:
            return NSLocalizedString(kCarEngineType1_6t, nil);
        default:
            return NSLocalizedString(kCarEngineType1_6td, nil);
    }
}

+ (nonnull NSString *)transTypeStringWithTransnType:(SPCarTransmissionType)transType
{
    switch (transType) {
        case SPCarTransmissionTypeManual:
            return NSLocalizedString(kCarTransmissionTypeManual, nil);
        case SPCarTransmissionTypeSemi_automatic:
            return NSLocalizedString(kCarTransmissionTypeSemi_automatic, nil);
        default:
            return NSLocalizedString(kCarTransmissionTypeaAtomatic, nil);
    }
}

+ (nonnull NSString *)stateTypeStringWithStateType:(SPCarStateType)stateType
{
    switch (stateType) {
        case SPCarStateTypeGood:
            return NSLocalizedString(kCarStateTypeGood, nil);
        case SPCarStateTypeNormal:
            return NSLocalizedString(kCarStateTypeNormal, nil);
        default:
            return NSLocalizedString(kCarStateTypeBad, nil);
    }
}

@end
