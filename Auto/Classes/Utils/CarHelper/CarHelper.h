//
//  CarHelper.h
//  Auto
//
//  Created by Svyat on 7/27/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CarModel.h"

static NSInteger const kSPCarEngineTypeCountItems = 6;
static NSInteger const kSPCarTransmissionTypeCountItems = 3;
static NSInteger const kSPCarStateTypeCountItems = 3;

typedef NS_ENUM(NSInteger, SPCarEngineType) {
    SPCarEngineType2ie,
    SPCarEngineType2t,
    SPCarEngineType2td,
    SPCarEngineType1_6ie,
    SPCarEngineType1_6t,
    SPCarEngineType1_6td
};

typedef NS_ENUM(NSInteger, SPCarTransmissionType) {
    SPCarTransmissionTypeManual,
    SPCarTransmissionTypeSemi_automatic,
    SPCarTransmissionTypeaAtomatic
};

typedef NS_ENUM(NSInteger, SPCarStateType) {
    SPCarStateTypeGood,
    SPCarStateTypeNormal,
    SPCarStateTypeBad
};

@interface CarHelper : NSObject

+ (nonnull NSNumber *)newIdCar;

+ (nonnull NSString *)engineTypeStringWithEngineType:(SPCarEngineType)engineType;
+ (nonnull NSString *)transTypeStringWithTransnType:(SPCarTransmissionType)transType;
+ (nonnull NSString *)stateTypeStringWithStateType:(SPCarStateType)stateType;

@end
