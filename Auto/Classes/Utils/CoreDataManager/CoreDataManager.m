//
//  SFCoreDataManager.m
//  karmaplatform
//
//  Created by Robert on 18.02.15.
//  Copyright (c) 2015 thinkmobiles. All rights reserved.
//

#import "CoreDataManager.h"
#import "NSURL+Extensions.h"

static NSString *const kModelName = @"CoreDataDB";
static NSString *const kModelExtension = @"momd";
static NSString *const kStorageName = @"CoreDatastorage.sqlite";

static NSString *const kCarModelEntityName = @"CarModel";
static NSString *const kWeatherModelEntityName = @"WeatherModel";

@interface CoreDataManager ()

@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation CoreDataManager

#pragma mark - public

+ (CoreDataManager *)sharedInstance
{
    static CoreDataManager *instance = nil;
    static dispatch_once_t dispatchOnceToken = 0;
    
    dispatch_once(&dispatchOnceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

#pragma mark - Public

- (NSManagedObject *)createManagedObjectWithClassName:(NSString *)className
{
    NSEntityDescription *entity =  [NSEntityDescription entityForName:className inManagedObjectContext:self.managedObjectContext];
    NSManagedObject *managedObject = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    
    return managedObject;
}

- (BOOL)addManagedObject:(NSManagedObject *)managedObject
{
    if (![managedObject managedObjectContext]) {
        NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
        if (managedObjectContext != nil) {
            [managedObjectContext insertObject:managedObject];
            [self saveContext];
            return YES;
        }
    } else {
        [self saveContext];
        return YES;
    }
    
    return NO;
}

- (void)deleteManagedObject:(NSManagedObject *)managedObject
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil && [managedObject managedObjectContext]) {
        [managedObjectContext deleteObject:managedObject];
        [self saveContext];
    }
}

- (NSArray *)getManagedObjectsFromClassName:(NSString *)className
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =  [NSEntityDescription entityForName:className inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSError *fetchError = nil;
    NSArray *resultsArray =[self.managedObjectContext executeFetchRequest:request error:&fetchError];
    assert(fetchError == nil);
    
    return resultsArray;
}

#pragma mark - Manage object

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            return;
        }
    }
}

- (void)undoContextChanges
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges]) {
            [managedObjectContext undo];
        }
    }
}

- (void)resetContextChanges
{
    [self.managedObjectContext reset];
}

#pragma mark - Private

- (NSManagedObjectContext*)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:kModelName withExtension:kModelExtension];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:kStorageName];
    NSError *error = nil;
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES};
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Private

- (NSURL *)applicationDocumentsDirectory
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *direcoryURLList = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    return direcoryURLList.lastObject;
}

@end
