//
//  SFCoreDataManager.h
//  karmaplatform
//
//  Created by Robert on 18.02.15.
//  Copyright (c) 2015 thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeatherModel.h"
#import "CarModel.h"

@interface CoreDataManager : NSObject

+ (instancetype)sharedInstance;

- (NSManagedObject *)createManagedObjectWithClassName:(NSString *)className;
- (BOOL)addManagedObject:(NSManagedObject *)managedObject;
- (void)deleteManagedObject:(NSManagedObject *)managedObject;
- (NSArray *)getManagedObjectsFromClassName:(NSString *)className;

- (void)saveContext;
- (void)undoContextChanges;
- (void)resetContextChanges;

@end
