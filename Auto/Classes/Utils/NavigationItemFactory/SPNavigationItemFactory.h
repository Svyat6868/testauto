//
//  SPNavigationItemFactory.h
//  SparkedApp
//
//  Created by Свят on 11/12/15.
//  Copyright © 2015 SocialMediaWave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNavigationItemFactory : NSObject

+ (UIBarButtonItem *)createLeftAdjustingItem;
+ (UIBarButtonItem *)createRightAdjustingItem;

+ (UIBarButtonItem *)createBackItem:(UIViewController *)viewController selector:(SEL)selector itemColor:(UIColor *)itemColor;
+ (UIBarButtonItem *)createAddItem:(UIViewController *)viewController selector:(SEL)selector itemColor:(UIColor *)itemColor;
+ (UIBarButtonItem *)createAddCarItem:(UIViewController *)viewController selector:(SEL)selector itemColor:(UIColor *)itemColor;


@end
