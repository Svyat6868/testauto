//
//  SPNavigationItemFactory.m
//  SparkedApp
//
//  Created by Свят on 11/12/15.
//  Copyright © 2015 SocialMediaWave. All rights reserved.
//

#import "SPNavigationItemFactory.h"
#import "UIImage+Extension.h"

const CGRect kButtonFrame = {{ 0.0f, 0.0f }, { 40.0f, 40.0f }};
const CGRect kAddCarButtonFrame = {{ 0.0f, 0.0f }, { 80.0f, 40.0f }};

@implementation SPNavigationItemFactory

+ (UIBarButtonItem *)createLeftAdjustingItem
{
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeparator.width = -12.f;
    return negativeSeparator;
}

+ (UIBarButtonItem *)createRightAdjustingItem
{
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeparator.width = -12.f;
    return negativeSeparator;
}

+ (UIBarButtonItem *)createBackItem:(UIViewController *)viewController selector:(SEL)selector itemColor:(UIColor *)itemColor
{
    UIImage *addImage = [UIImage imageNamed:@"backImage"];
    return [SPNavigationItemFactory imageBarButtonItem:viewController selector:selector itemColor:itemColor image:addImage];
}

+ (UIBarButtonItem *)createAddItem:(UIViewController *)viewController selector:(SEL)selector itemColor:(UIColor *)itemColor
{
    UIImage *addImage = [UIImage imageNamed:@"addImageImage"];
    return [SPNavigationItemFactory imageBarButtonItem:viewController selector:selector itemColor:itemColor image:addImage];
}

+ (UIBarButtonItem *)createAddCarItem:(UIViewController *)viewController selector:(SEL)selector itemColor:(UIColor *)itemColor
{
    UIButton *addCarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addCarButton.frame = kAddCarButtonFrame;
    
    UIColor *addCarHighlightedItemColor = [itemColor colorWithAlphaComponent:0.5];
    [addCarButton setTitleColor:itemColor forState:UIControlStateNormal];
    [addCarButton setTitleColor:addCarHighlightedItemColor forState:UIControlStateHighlighted];
    [addCarButton setTitle:NSLocalizedString(@"AddCarViewControllerButtonAdd", nil) forState:UIControlStateNormal];
    
    if (viewController && [viewController respondsToSelector:selector]) {
        [addCarButton addTarget:viewController action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc] initWithCustomView:addCarButton];
    return addItem;
}

#pragma mark - Private

+ (UIBarButtonItem *)imageBarButtonItem:(UIViewController *)viewController selector:(SEL)selector itemColor:(UIColor *)itemColor image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = kButtonFrame;
    button.tintColor = itemColor;
    
    UIImage *newImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *highlightedImage = [UIImage image:newImage withAlpha:0.5];
    
    [button setImage:newImage forState:UIControlStateNormal];
    [button setImage:highlightedImage forState:UIControlStateHighlighted];
    
    if (viewController && [viewController respondsToSelector:selector]) {
        [button addTarget:viewController action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIBarButtonItem *imageBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    return imageBarButtonItem;
}

@end
