//
//  CarTableViewCell.h
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarTableViewCell : UITableViewCell

- (void)fillCellWithCar:(CarModel *)car;

@end
