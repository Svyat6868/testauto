//
//  CarTableViewCell.m
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "CarTableViewCell.h"

@interface CarTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *carDLabel;
@property (weak, nonatomic) IBOutlet UILabel *pricaDLabel;

@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *carPriceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *carImageView;

@end

@implementation CarTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.carDLabel.text = NSLocalizedString(@"CarTableViewCellCarLabel", nil);
    self.pricaDLabel.text = NSLocalizedString(@"CarTableViewCellPriceLabel", nil);;
}

#pragma mark - Public

- (void)fillCellWithCar:(CarModel *)car
{
    self.carModelLabel.text = car.model;
    self.carPriceLabel.text = [NSString stringWithFormat:@"%ld", [car.price integerValue]];

    if ([car.images count] > 0) {
        self.carImageView.image = [car.images firstObject];
    }
}

@end
