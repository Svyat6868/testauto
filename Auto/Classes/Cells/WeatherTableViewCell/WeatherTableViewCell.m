//
//  WeatherTableViewCell.m
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "WeatherTableViewCell.h"

@interface WeatherTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *weatherTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;

@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation WeatherTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - Public

- (void)fillCellWithWeather:(WeatherModel *)weather
{
    self.weatherTypeImageView.image = [weather weatherTypeImage];
    
    if (weather.precipitation != nil) {
        self.weatherLabel.text = [NSString stringWithFormat:@"%@ %@ %@", [weather weatherTypeString], NSLocalizedString(@"Precipitation", nil), weather.precipitation];
    } else {
        self.weatherLabel.text = [weather weatherTypeString];
    }
    
    NSString *parameterTemperature = [weather.temperature integerValue] >= 0 ? @"+" : @"-";
    self.temperatureLabel.text = [NSString stringWithFormat:@"%@%ld",parameterTemperature, [weather.temperature integerValue]];
    
    self.placeLabel.text = weather.place;
    self.dateLabel.text = [NSDateFormatter localizedStringFromDate:weather.date dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterLongStyle];
}

@end
