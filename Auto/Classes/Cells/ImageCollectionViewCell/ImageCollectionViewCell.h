//
//  ImageCollectionViewCell.h
//  Auto
//
//  Created by Svyat on 7/27/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
