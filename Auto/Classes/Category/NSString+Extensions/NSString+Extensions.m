//
//  NSString+Extensions.m
//  ShiwaForce_iOS
//
//  Created by Robert on 18.11.14.
//  Copyright (c) 2014 thinkmobiles. All rights reserved.
//

#import "NSString+Extensions.h"
#import <CoreFoundation/CoreFoundation.h>

static NSString *const kCharacterToURLEncode = @"!*'();@&=+$,%[]";
static NSString *const kCharacterNotToURLEncode = @"#";

@implementation NSString (Extensions)

#pragma clang diagnostic ignored "-Wdeprecated-declarations"

- (NSString*)kpUrlEncodedString
{
    CFAllocatorRef allocator = NULL;
    CFStringRef originalString = (__bridge CFStringRef)self;
    CFStringRef charactersToLeaveUnescaped = (__bridge CFStringRef)kCharacterNotToURLEncode;
    CFStringRef legalURLCharactersToBeEscaped = (__bridge CFStringRef)kCharacterToURLEncode;
    
    CFStringRef encodedString = CFURLCreateStringByAddingPercentEscapes(allocator,
                                                                        originalString,
                                                                        charactersToLeaveUnescaped,
                                                                        legalURLCharactersToBeEscaped,
                                                                        kCFStringEncodingUTF8);
    return (NSString*)CFBridgingRelease(encodedString);
}

- (NSString*)kpUrlDecodedString
{
    CFAllocatorRef allocator = NULL;
    CFStringRef originalString = (__bridge CFStringRef)self;

    CFStringRef charsToLeaveEscaped = CFSTR("");
    
    CFStringRef decodedString = CFURLCreateStringByReplacingPercentEscapesUsingEncoding(allocator, originalString, charsToLeaveEscaped, kCFStringEncodingUTF8);
    return (NSString*)CFBridgingRelease(decodedString);
}

@end
