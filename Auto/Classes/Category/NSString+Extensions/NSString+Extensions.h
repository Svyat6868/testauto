//
//  NSString+Extensions.h
//  ShiwaForce_iOS
//
//  Created by Robert on 18.11.14.
//  Copyright (c) 2014 thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)

- (NSString*)kpUrlEncodedString;
- (NSString*)kpUrlDecodedString;

@end
