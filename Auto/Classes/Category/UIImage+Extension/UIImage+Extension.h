//
//  UIImage+Extension.h
//  Skratch
//
//  Created by Свят on 10/28/15.
//  Copyright © 2015 PikaPika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

+ (UIImage*)image:(UIImage *)image withAlpha:(CGFloat)alpha;

@end
