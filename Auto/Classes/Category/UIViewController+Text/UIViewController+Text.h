//
//  UIViewController+Text.h
//  bsparked
//
//  Created by Svyat on 5/12/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Text)

- (void)addInputAccessoryViewInTextField:(UITextField *)textField target:(id)target action:(SEL)action;
- (void)addInputAccessoryViewInTextView:(UITextView *)textView target:(id)target action:(SEL)action;

@end
