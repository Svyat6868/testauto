//
//  UIViewController+Text.m
//  bsparked
//
//  Created by Svyat on 5/12/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import "UIViewController+Text.h"

static CGFloat const kHeightButtonInputAccView = 40.f;
static CGFloat const kWidthButtonInputAccView = 80.f;

@implementation UIViewController (Text)

- (void)addInputAccessoryViewInTextField:(UITextField *)textField target:(id)target action:(SEL)action
{
    UIView *inputAccView = [self inputAccessoryViewFromTarget:target action:action];
    [textField setInputAccessoryView:inputAccView];
}

- (void)addInputAccessoryViewInTextView:(UITextView *)textView target:(id)target action:(SEL)action
{
    UIView *inputAccView = [self inputAccessoryViewFromTarget:target action:action];
    [textView setInputAccessoryView:inputAccView];
}

#pragma mark - Private

- (UIView *)inputAccessoryViewFromTarget:(id)target action:(SEL)action
{
    UIView *inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, kHeightButtonInputAccView)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 0.8];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor grayColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [inputAccView addSubview:btnDone];
    
    btnDone.translatesAutoresizingMaskIntoConstraints = NO;
    [inputAccView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnDone]-0-|" options:0 metrics:nil views:@{@"btnDone" : btnDone}]];
    [inputAccView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnDone]-0-|" options:0 metrics:nil views:@{@"btnDone" : btnDone}]];
    [btnDone addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[btnDone(%f)]",kWidthButtonInputAccView] options:0 metrics:nil views:@{@"btnDone" : btnDone}]];
    return inputAccView;
}



@end
