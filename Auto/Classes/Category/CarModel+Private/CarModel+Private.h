//
//  CarModel+Private.h
//  Auto
//
//  Created by Boost Ser on 1/11/18.
//  Copyright © 2018 Svyat. All rights reserved.
//

#import "CarModel.h"

@interface CarModel (Private)

- (void)setCarImages:(nonnull NSArray *)carImages;

@end
