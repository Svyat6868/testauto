//
//  NSURL+Extensions.h
//  karmaplatform
//
//  Created by Admin on 28.04.15.
//  Copyright (c) 2015 thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Extensions)

- (NSURL*)rootURL;
- (NSString*)rootUrlString;
- (NSURL*)URLByAppendingKPPAthComponent:(NSString*)pathComponent;

@end
