//
//  NSURL+Extensions.m
//  karmaplatform
//
//  Created by Admin on 28.04.15.
//  Copyright (c) 2015 thinkmobiles. All rights reserved.
//


#import "NSURL+Extensions.h"
#import "NSString+Extensions.h"

static NSString *const kQuerySeparator  = @"&";
static NSString *const kQueryDivider    = @"=";
static NSString *const kQueryBegin      = @"?";
static NSString *const kFragmentBegin   = @"#";

@implementation NSURL (Extensions)

- (NSURL*)rootURL
{
    NSString *rootUrlString = [self rootUrlString];
    
    if (rootUrlString.length > 0) {
        return [NSURL URLWithString:rootUrlString];
    } else {
        return nil;
    }
}

- (NSString*)rootUrlString
{
    if (self.host.length > 0 && self.scheme.length > 0) {
        return [NSString stringWithFormat:@"%@://%@", self.scheme, self.host];
    } else {
        return nil;
    }
}

- (NSURL*)URLByAppendingKPPAthComponent:(NSString*)pathComponent
{
    NSParameterAssert([pathComponent isKindOfClass:[NSString class]]);
    
    NSURL *rootURL = [self copy];
    NSURL *pathComponentURL = [NSURL URLWithString:pathComponent];
    
    if (!pathComponentURL) {
        pathComponentURL = [NSURL URLWithString:[pathComponent kpUrlEncodedString] ];
    }
    
    NSURLComponents *rootUrlComponents = [NSURLComponents componentsWithURL:rootURL resolvingAgainstBaseURL:YES];
    NSURLComponents *pathComponents = [NSURLComponents componentsWithURL:pathComponentURL resolvingAgainstBaseURL:YES];
    
    NSArray *rootURLQueryItems = rootUrlComponents.queryItems;
    NSArray *pathCompontnQueryItems = pathComponents.queryItems;
    
    if (rootURLQueryItems.count != 0 || pathCompontnQueryItems.count != 0) {
        NSMutableArray *mutablePathComponentQueryItems = (pathCompontnQueryItems.count > 0) ? [pathCompontnQueryItems mutableCopy] : [NSMutableArray array];
        NSArray *rootURLQueryItemsToAppend = (rootURLQueryItems.count > 0) ? rootURLQueryItems : @[];
        [mutablePathComponentQueryItems addObjectsFromArray:rootURLQueryItemsToAppend];
        rootUrlComponents.queryItems = nil;
        rootUrlComponents.query = nil;
        pathComponents.queryItems = mutablePathComponentQueryItems;
    }
    
    //
    // Path components shouldnt start with "/"
    //
    if ([pathComponents.path hasPrefix:@"/"]) {
        NSMutableString *tmpPath = [pathComponents.path mutableCopy];
        [tmpPath deleteCharactersInRange:NSMakeRange(0, 1)];
        pathComponents.path = tmpPath;
    }
    
    //
    //  Root URL path should start with "/"
    //
    if (rootUrlComponents.path.length > 0 && ![[rootUrlComponents path] hasSuffix:@"/"]) {
        NSString *newPath = [NSString stringWithFormat:@"%@/", rootUrlComponents.path];
        rootUrlComponents.path = newPath;
    }
    
    NSURL *processedRootURL = [rootUrlComponents URL];
    NSURL *processedPathComponent = [pathComponents URL];
    
    NSURL *finalURL = [NSURL URLWithString:processedPathComponent.absoluteString relativeToURL:processedRootURL].absoluteURL;
    return finalURL;
}



@end
