//
//  AppDelegate.h
//  Auto
//
//  Created by Svyat on 7/21/16.
//  Copyright © 2016 Svyat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message viewController:(UIViewController *)viewController;

@end

